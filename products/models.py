from django.db import models
from django.urls import reverse
import json
import pymorphy2

from products.specifications_lib import lib


class Product(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    price = models.IntegerField()
    specifications = models.TextField()
    category = models.ForeignKey('Category', models.SET_NULL, null=True, default=None, blank=True)
    description = models.TextField(null=True)

    def __str__(self):
        return self.specifications

    class Meta:
        ordering = ['-name']

    def get_absolute_url(self):
        return reverse("ProductDetailPage", kwargs={"pk": self.pk})

    def get_specification(self):
        return json.loads(self.specifications).values()

    def get_headers_specification(self):
        return json.loads(self.specifications).keys()

    @property
    def pretty_name(self):
        return ' '.join([v for v in json.loads(self.specifications).values()])

    @property
    def text(self):

        CASES_CHOISES = {
            'femn': 'accs',
            'masc': 'nomn',
            'neut': 'accs',
        }

        res = []
        for key, val in json.loads(self.specifications).items():
            tmplt = lib.get(key, None)
            from .myre import text_generator
            tmplt = text_generator(tmplt)
            if tmplt:
                if key == 'Вид металла':
                    morph = pymorphy2.MorphAnalyzer()
                    words = val.split(' ')
                    words = [word.strip().lower() for word in words]
                    pm_words = [morph.parse(word) for word in words]
                    # TODO: мужской род, женский род, существительное
                    gender = None
                    padej = 'accs'
                    for word in enumerate(pm_words):
                        if word[1][0].tag.POS == 'NOUN':  # Если существительное меняем падеж в зависимости от Рода
                            padej = CASES_CHOISES.get(word[1][0].tag.gender)
                            words[word[0]] = word[1][0].inflect({padej, 'sing'}).word
                            break
                    # words = [word[0].inflect({padej, 'sing'}) for word in pm_words]
                    # res.append(tmplt.format(' '.join([word.word for word in words if word is not None])))
                    res.append(tmplt.format(' '.join(words)))
                    continue
                res.append(tmplt.format(val.strip().lower()))
        return '. '.join(res)


class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=True, null=True)
    parent_category = models.ForeignKey('Category', on_delete=models.SET_NULL, null=True, blank=True)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    # class Meta:
    #     ordering = ['name']

    def get_highest_price(self):
        try:
            hightes_price = self.product_set.all().order_by('-price').first().price
        except AttributeError:
            hightes_price = 0
        return hightes_price

    def get_cheapest_price(self):
        try:
            cheapest_price = self.product_set.all().order_by('price').first().price
        except AttributeError:
            cheapest_price = 0
        return cheapest_price

    def get_parents(self, reverse=False) -> list:
        parents = []
        current_category = self
        while current_category.parent_category:
            parents.append(current_category.parent_category)
            current_category = current_category.parent_category
        if reverse:
            return parents[::-1]
        return parents

    def _category_child_handler(self, cat):
        result = Category.objects.none()
        if cat.category_set.exists():
            child_categoies = cat.category_set.prefetch_related('category_set').all()
            result = result.union(child_categoies)
            for category in cat.category_set.all():
                comeon = self._category_child_handler(category)
                if comeon.exists():
                    result = result.union(comeon)
        return result

    @property
    def childs(self):
        if self.parent_category is None:
            return Category.objects.none()
        return self._category_child_handler(self)

    @property
    def length(self):
        return len(self.name)

    def get_steel_grades(self):
        grades = {}
        for product in self.product_set.all():
            specs = json.loads(product.specifications)
            grade = specs.get('Марка', None)
            if grade is not None:
                valid_grade = grade.strip()
                grades.setdefault(valid_grade, 0)
                grades[valid_grade] += 1
        return [g[0] for g in sorted(grades.items(), key=lambda x: x[1], reverse=True)]

    def get_gosts(self):
        grades = {}
        for product in self.product_set.all():
            specs = json.loads(product.specifications)
            grade = specs.get('ГОСТ, ОСТ, ТУ', None)
            if grade is not None:
                valid_grade = grade.strip()
                grades.setdefault(valid_grade, 0)
                grades[valid_grade] += 1
        return [g[0] for g in sorted(grades.items(), key=lambda x: x[1], reverse=True)]

    def get_absolute_url(self):
        return reverse("CategoryPage", kwargs={"slug": self.slug})
