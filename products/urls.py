from django.urls import path, include

from . import views
from .views import CatgoryListView, ContactFormView

urlpatterns = [
    path('', views.IndexPage.as_view(), name='IndexPage'),
    path('category/<str:slug>/', views.CategoryDetailView.as_view(), name='CategoryPage'),
    path('category-list/', CatgoryListView.as_view(), name='category_list'),
    path('sku/<int:pk>/', views.ProductDetailView.as_view(), name='ProductDetailPage'),
    path("order/", views.ContactFormView.as_view(), name="OrderPage"),
    path("thanks/", views.ThanksView.as_view(), name="ThanksPage")
]
