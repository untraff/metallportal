from datetime import datetime
from django.contrib.sitemaps import Sitemap
from .models import Category, Product


class CategorySitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    limit = 1000

    def items(self):
        return Category.objects.all()

    def lastmod(self, obj):
        return datetime.now()


class ProductSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    limit = 1000

    def items(self):
        return Product.objects.all()

    def lastmod(self, obj):
        return datetime.now()
