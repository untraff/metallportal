from django.db.models import Count
from django.views.generic.base import ContextMixin


class ChildCategoryContextMixin(ContextMixin):
    
    model = None
    object = None
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.child_categorys = []
        self.child_products = []

    def get_child_categorys(self):
        self.child_categorys = self.model.objects.filter(parent_category=self.object).annotate(
            products_count=Count('product'),
            categorys_count=Count('category'),
        )
        return sorted([x for x in self.child_categorys], key=lambda cat: len(cat.name), reverse=True)

    def get_child_products(self):
        for child_category in self.child_categorys:
            self.child_products.append(child_category.product_set.all())
        return self.child_products

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'child_categorys': self.get_child_categorys(),
            # 'child_products': self.get_child_products()  # TODO: для продуктов
        })
        return context
