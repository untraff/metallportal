import re
import random


def text_generator(text):
    if text is None:
        return None

    result = set()
    pattern = '^(.*)<<<([^<<<>>>]+)>>>(.*)$'

    def engine(text: str):
        prog = re.compile(pattern)
        nonlocal result
        if prog.match(text):
            matches = prog.findall(text)
            for variant in matches[0][1].split('|'):
                engine(f'{matches[0][0]}{variant}{matches[0][2]}')
        else:
            result.add(text)
    engine(text)
    return random.choice(list(result))
