from custompages.models import Page

COMPANY_NAME = 'МеталлКаталог'
DOMAIN = 'metallcatalog.ru'


class MainPageMenuItem:
    slug = '/'
    name = 'Главная'


MENU = [p for p in Page.objects.all()]


DATA_LIB = {
    'COMPANY_NAME': COMPANY_NAME,
    'PHONE': '<a class="phone" href="tel:88126129157"><nobr>8 812 612 91 57</nobr></a>',
    'PHONE_SHORT': '8 812 612 91 57',
    'EMAIL': f'<a class="email" href="mailto:info@{DOMAIN}">info@{DOMAIN}</a>',
    'EMAIL_SHORT': f'info@{DOMAIN}',
    'ADDRESS_FULL': 'г. Санкт-Петербург, 196240, 6-й Предпортовый проезд, д. 8.',
    'ADDRESS_SHORT': 'СПб, 6-й Предпортовый проезд, д. 8.',
    'MAIN_MENU_ITEMS': MENU,
}


def global_settings(request):
    return DATA_LIB
