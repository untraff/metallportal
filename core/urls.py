from django.contrib import admin
from django.urls import path, include
from django.contrib.sitemaps.views import sitemap
from .views import robots, custom_sitemap
from products.sitemaps import ProductSitemap, CategorySitemap
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('products.urls')),
    path('robots.txt', robots),
    path('product-sitemap.xml', sitemap, {'sitemaps': {'ProductSitemap': ProductSitemap}},
         name='django.contrib.sitemaps.views.sitemap'),
    path('category-sitemap.xml', sitemap, {'sitemaps': {'CategorySitemap': CategorySitemap}},
         name='django.contrib.sitemaps.views.sitemap'),
    path('customsitemap.xml', custom_sitemap),
    path('pages/', include('django.contrib.flatpages.urls')),
    path('custom/', include('custompages.urls'))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
