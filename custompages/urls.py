from django.urls import path

from . import views
from .views import PageDetailView

urlpatterns = [
    # path('', views.IndexPage.as_view(), name='IndexPage'),
    path('<str:slug>/', views.PageDetailView.as_view(), name='PageDetail'),
]
