from bs4 import BeautifulSoup, PageElement
import os
import time
import requests
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Parser:
    table_selector = 'table.Tab_AddTovar7'
    page_counter_selector = '.block_page'
    folder = os.path.join(BASE_DIR, 'scrapper', 'load_data')

    def __init__(self, url_data: dict):
        self.main_cat_url = url_data['link']
        self.page_name = url_data['slug']
        self.id = url_data['id']
        self.page_urls = []

    def _load_pages_for_parse(self):
        return

    def _get_table_head(self, tr) -> list:
        data = [th.text for th in tr.descendants if th.name == 'th']
        return data[:-2]

    def _get_rows(self, table: PageElement) -> list:
        return [e for e in table.descendants if e.name == 'tr']

    def _get_page_counter(self, soup) -> int:
        try:
            return int(soup.select(self.page_counter_selector)[-1].text)
        except IndexError:
            raise Exception('LOL?! Something wrong with page counter')

    def _get_tds(self, tr) -> list:
        data = [td.text for td in tr.descendants if td.name == 'td']
        return data[:-2]

    def _wirte_in_csv(self, data: list, output_filename='output.csv'):
        with open(os.path.join(self.folder, output_filename), 'a') as file_stream:
            file_stream.write(';'.join(data) + '\n')

    def _create_page_urls(self, pages_count: int) -> list:
        return [self.main_cat_url + 'page/{}/'.format(num) for num in range(1, pages_count + 1)]

    def make_filename(self, object_name):
        print('{}___{}.csv'.format(object_name, datetime.now().strftime('%Y-%m-%d--%H-%M-%S')))
        return '{}___{}.csv'.format(object_name, datetime.now().strftime('%Y-%m-%d--%H-%M-%S'))

    def run_parse(self):
        content = requests.get(self.main_cat_url).text
        bs = BeautifulSoup(content, 'lxml')
        pages_count = self._get_page_counter(bs)
        found_th = False
        self.page_urls = self._create_page_urls(pages_count)
        filename = self.make_filename(self.page_name)

        for page_url in self.page_urls:

            html_content = requests.get(page_url).text
            page_soup = BeautifulSoup(html_content, 'lxml')

            table = page_soup.select_one(self.table_selector)

            trs = self._get_rows(table)

            if not found_th:
                table_header = self._get_table_head(trs[0])
                self._wirte_in_csv(table_header, output_filename=filename)
                self._wirte_in_csv(table_header, output_filename='all.csv')
                found_th = True

            for tr in trs:
                required_data = self._get_tds(tr)
                if required_data:
                    self._wirte_in_csv(required_data, output_filename=filename)
                    self._wirte_in_csv(required_data, output_filename='all.csv')

            time.sleep(2)
        print('done')


def main():
    urls = []
    with open("urls_for_parse2.csv", 'r', encoding='utf-8') as sfile:
        for row in sfile.readlines():
            print(row.strip().split(','))
            id, link, slug = row.strip().split(',')
            urls.append(
                {'id': id, 'link': f'{link}/', 'slug': slug}
            )
    while urls:
        url_data = urls.pop()
        try:
            parser = Parser(url_data)
            parser.run_parse()
        except requests.exceptions.MissingSchema:
            pass


if __name__ == '__main__':
    main()



