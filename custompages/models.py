from django.db import models


class Page(models.Model):
    title = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True, blank=True)
    h1 = models.CharField(max_length=255)
    seo_text = models.TextField()
    ordering_in_menu = models.IntegerField()
    slug = models.SlugField()

    def __str__(self):
        return f'{self.name} {self.slug}'

