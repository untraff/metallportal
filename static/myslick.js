$(document).ready(function () {
    $('.your-class').slick({
        // dots: true,
        infinite: true,
        speed: 500,
        // centerMode: true,

        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
});
