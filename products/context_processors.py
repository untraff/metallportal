from products.models import Category


class Menu:
    objects = Category.objects.filter(parent_category=None)

    @classmethod
    def get_menu(cls):
        return cls.objects



def global_settings(request):
    return {
        'menu': Menu.get_menu(),
    }
