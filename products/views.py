import json

from django.core.paginator import Paginator
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.urls import reverse

from core import settings
from products.forms import ContactForm
from products.mixins import ChildCategoryContextMixin
from products.models import Product, Category
from schemaorg.handlers import Schema, SchemaBreadCrumb
from core.context_processors import DATA_LIB


class IndexPage(ListView):
    model = Product

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        categories_count = Category.objects.all().count()
        products_count = Product.objects.all().count()
        company = DATA_LIB['COMPANY_NAME']
        data = {
            'h1': f'{company}. Все виды металла от лидеров металлургии',
            'title': 'Металлобаза в Москве, Санкт-Петербурге и других городах россии',
            'description': 'Продажа всех видов товаров, работаем с 1991, лучшие цены в России',
            'categories_count': categories_count,
            'products_count': products_count,
        }
        context.update(data)
        return context


class MenuView(ListView):
    model = Category


class CatgoryListView(ListView):
    model = Category
    paginate_by = 100


class CategoryDetailView(ChildCategoryContextMixin, DetailView):
    """Страница категории
    """
    model = Category
    schema_class = Schema
    paginate_by = 100

    def _prepare_json(self):
        url = reverse('CategoryPage', kwargs={'slug': self.object.slug})

        hightes_price = self.object.get_highest_price()
        cheapest_price = self.object.get_cheapest_price()
        url_host = self.request.headers['Host']
        offer_urls = []
        for product in self.object.product_set.all():
            product_short_url = reverse('ProductDetailPage', kwargs={'pk': product.pk})
            product_full_url = f'{self.request.scheme}://{url_host}{product_short_url}'
            offer_urls.append(product_full_url)

        schema_data = self.schema_class(
            self.object.name, url, hightes_price=hightes_price, cheapest_price=cheapest_price, offer_urls=offer_urls,
            description=self.object.name, sku=self.object.pk, image=settings.SCHEMA_DEFAULT_PRODUCT_IMG)

        return json.dumps(schema_data.get_aggregate_offers(), ensure_ascii=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()

        breadcrumbs = SchemaBreadCrumb()
        json_ = self._prepare_json()

        html_breadcrumbs = []
        url_host = self.request.headers['Host']
        for item in self.object.get_parents(reverse=True):
            parent_cat_uri = reverse("CategoryPage", kwargs={"slug": item.slug})
            page = f'{self.request.scheme}://{url_host}{parent_cat_uri}'
            breadcrumbs.add_breadcrumb(page, item.name)
            html_breadcrumbs.append({'url': item.slug, 'anchor': item.name})
        page = f'{self.request.scheme}://{url_host}{self.request.path}'
        breadcrumbs.add_breadcrumb(page, self.object.name)

        products = self.object.product_set.all()

        paginator = Paginator(products, 100)
        page_number = self.request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        grades = self.object.get_steel_grades()

        gosts = self.object.get_gosts()

        context.update({
            'products': products,
            'json': json_,
            'json_breadcrumbs': breadcrumbs.get_json_schema(),
            'html_breadcrumbs': html_breadcrumbs,
            'h1': f'{self.object.name}',
            # 'sub_h1': f'Всего {self.object.childs.count()} категорий в {self.object.category_set.all().count()} дочерних',
            'title': f'{self.object.name} купить в Москве и Санкт-Петербурге',
            'page_obj': page_obj,
            'grades': grades,
            'gosts': gosts,
        })
        return context


class ProductDetailView(DetailView):
    model = Product
    schema_class = Schema

    def _prepare_specifications(self):
        return json.loads(self.object.specifications)

    def get_json_schema(self):
        name = self.object.pretty_name
        url = reverse("ProductDetailPage", kwargs={"pk": self.object.pk})
        return json.dumps(Schema(name, url, description=self.object.description).get_offer(), ensure_ascii=False)

    def create_json_breadcrumbs(self):
        breadcrumbs = SchemaBreadCrumb()
        url_host = self.request.headers['Host']
        for item in self.parents:
            category_uri = reverse("CategoryPage", kwargs={'slug': item.slug})
            page = f'{self.request.scheme}://{url_host}{category_uri}'
            breadcrumbs.add_breadcrumb(page, item.name)
        page = f'{self.request.scheme}://{url_host}{self.request.path}'
        breadcrumbs.add_breadcrumb(page, self.object.pretty_name)
        return breadcrumbs.get_json_schema()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()

        html_breadcrumbs = []

        self.parents = self.object.category.get_parents(reverse=True)

        for item in self.object.category.get_parents(reverse=True):
            html_breadcrumbs.append({'url': item.slug, 'anchor': item.name})

        context.update({
            'spec_data': self._prepare_specifications(),
            'title': f'{self.object.pretty_name} купить в СПб и Москве',
            'h1': self.object.pretty_name,
            'html_breadcrumbs': html_breadcrumbs,
            'json_breadcrumbs': self.create_json_breadcrumbs(),
            'json': self.get_json_schema()
        })

        return context


class ContactFormView(FormView):
    form_class = ContactForm
    template_name = "contact_form.html"
    success_url = "/thanks/"

    def form_valid(self, form: ContactForm):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_email()
        return super().form_valid(form)


class ThanksView(TemplateView):
    template_name = "thanks.html"
