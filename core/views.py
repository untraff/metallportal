from django.http import HttpResponse
import os


def robots(request):
    return HttpResponse('User-agent: *\nAllow: /', content_type="text/plain")


def custom_sitemap(request):
    from django.conf import settings

    filename = os.path.join(settings.BASE_DIR, *['static', 'sitemap.xml'])
    with open(filename) as fp:
        return HttpResponse(fp.read(), content_type="application/xml")
